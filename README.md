# Description

A top-down shooter game.

## Built with

- [TypeScript](https://www.typescriptlang.org/)
- [PIXI.js](https://www.pixijs.com/)
- also: [webpack](https://webpack.js.org/), [TexturePacker](https://www.codeandweb.com/texturepacker)

## Disclaimer
This is a non-commercial project. I do not own any rights to the images used in this project. All character and monster sprites belong to [Riley Gombart](https://opengameart.org/users/rileygombart) and were downloaded from [OpenGameArt.org](https://opengameart.org/content/animated-top-down-survivor-player). Tiles textures come from the game "Doom" and "Doom 2" by [id Software](https://www.idsoftware.com). I therefore derive no financial advantage from this project.

# Installation

## Prerequisites

You will require `Node.js` to run this program. Also `npm` is highly recommended in order to easily install all dependencies and packages.

## Installation

To install required packages, go into main directory and run command:

```
npm install
```

# Compilation
To compile all source files run command:

```
npm build
```

to launch Webpack and TypeScript compiler. All files should compile into `main.js` located at `dist/js/`. To compile in `watch` mode run command:

```
npm start
```

# Run

To run the game just open `index.html` file in the browser after successful compilation.