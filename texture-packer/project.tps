<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.1.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs4</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../dist/spritesheet/spritesheet.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">images/floor/floor1.png</key>
            <key type="filename">images/wall/wall1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/player/feet/idle/0.png</key>
            <key type="filename">images/player/feet/idle/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,39,66,77</rect>
                <key>scale9Paddings</key>
                <rect>33,39,66,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/player/feet/run/0.png</key>
            <key type="filename">images/player/feet/run/1.png</key>
            <key type="filename">images/player/feet/run/10.png</key>
            <key type="filename">images/player/feet/run/11.png</key>
            <key type="filename">images/player/feet/run/12.png</key>
            <key type="filename">images/player/feet/run/13.png</key>
            <key type="filename">images/player/feet/run/14.png</key>
            <key type="filename">images/player/feet/run/15.png</key>
            <key type="filename">images/player/feet/run/16.png</key>
            <key type="filename">images/player/feet/run/17.png</key>
            <key type="filename">images/player/feet/run/18.png</key>
            <key type="filename">images/player/feet/run/19.png</key>
            <key type="filename">images/player/feet/run/2.png</key>
            <key type="filename">images/player/feet/run/3.png</key>
            <key type="filename">images/player/feet/run/4.png</key>
            <key type="filename">images/player/feet/run/5.png</key>
            <key type="filename">images/player/feet/run/6.png</key>
            <key type="filename">images/player/feet/run/7.png</key>
            <key type="filename">images/player/feet/run/8.png</key>
            <key type="filename">images/player/feet/run/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,31,102,62</rect>
                <key>scale9Paddings</key>
                <rect>51,31,102,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/0.png</key>
            <key type="filename">images/weapon/knife/attack/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.414634,0.373333</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/10.png</key>
            <key type="filename">images/weapon/knife/attack/11.png</key>
            <key type="filename">images/weapon/knife/attack/7.png</key>
            <key type="filename">images/weapon/knife/attack/8.png</key>
            <key type="filename">images/weapon/knife/attack/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.463415,0.373333</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.45122,0.373333</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/13.png</key>
            <key type="filename">images/weapon/knife/attack/6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.439024,0.373333</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/14.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.426829,0.373333</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/2.png</key>
            <key type="filename">images/weapon/knife/attack/3.png</key>
            <key type="filename">images/weapon/knife/attack/4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.402439,0.373333</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/attack/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.390244,0.386667</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/idle/0.png</key>
            <key type="filename">images/weapon/knife/idle/1.png</key>
            <key type="filename">images/weapon/knife/idle/10.png</key>
            <key type="filename">images/weapon/knife/idle/11.png</key>
            <key type="filename">images/weapon/knife/idle/12.png</key>
            <key type="filename">images/weapon/knife/idle/13.png</key>
            <key type="filename">images/weapon/knife/idle/14.png</key>
            <key type="filename">images/weapon/knife/idle/15.png</key>
            <key type="filename">images/weapon/knife/idle/16.png</key>
            <key type="filename">images/weapon/knife/idle/17.png</key>
            <key type="filename">images/weapon/knife/idle/18.png</key>
            <key type="filename">images/weapon/knife/idle/19.png</key>
            <key type="filename">images/weapon/knife/idle/2.png</key>
            <key type="filename">images/weapon/knife/idle/3.png</key>
            <key type="filename">images/weapon/knife/idle/4.png</key>
            <key type="filename">images/weapon/knife/idle/5.png</key>
            <key type="filename">images/weapon/knife/idle/6.png</key>
            <key type="filename">images/weapon/knife/idle/7.png</key>
            <key type="filename">images/weapon/knife/idle/8.png</key>
            <key type="filename">images/weapon/knife/idle/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,14,36,28</rect>
                <key>scale9Paddings</key>
                <rect>18,14,36,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/weapon/knife/run/0.png</key>
            <key type="filename">images/weapon/knife/run/1.png</key>
            <key type="filename">images/weapon/knife/run/10.png</key>
            <key type="filename">images/weapon/knife/run/11.png</key>
            <key type="filename">images/weapon/knife/run/12.png</key>
            <key type="filename">images/weapon/knife/run/13.png</key>
            <key type="filename">images/weapon/knife/run/14.png</key>
            <key type="filename">images/weapon/knife/run/15.png</key>
            <key type="filename">images/weapon/knife/run/16.png</key>
            <key type="filename">images/weapon/knife/run/17.png</key>
            <key type="filename">images/weapon/knife/run/18.png</key>
            <key type="filename">images/weapon/knife/run/19.png</key>
            <key type="filename">images/weapon/knife/run/2.png</key>
            <key type="filename">images/weapon/knife/run/3.png</key>
            <key type="filename">images/weapon/knife/run/4.png</key>
            <key type="filename">images/weapon/knife/run/5.png</key>
            <key type="filename">images/weapon/knife/run/6.png</key>
            <key type="filename">images/weapon/knife/run/7.png</key>
            <key type="filename">images/weapon/knife/run/8.png</key>
            <key type="filename">images/weapon/knife/run/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,14,35,27</rect>
                <key>scale9Paddings</key>
                <rect>18,14,35,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>images/floor</filename>
            <filename>images/wall</filename>
            <filename>images/player</filename>
            <filename>images/weapon</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
