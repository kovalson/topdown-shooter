import Game from "./class/Game";

window.onload = () => {
  const baseWidth = 1000;
  new Game({
    baseWidth: baseWidth
  });
};