interface IWorldConfig {
  rows: number,
  columns: number,
  textures: PIXI.ITextureDictionary,
  obstacleBorders?: boolean
}

export default IWorldConfig;