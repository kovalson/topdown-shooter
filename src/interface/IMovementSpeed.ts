interface IMovementSpeed {
  min: number,
  max: number,
  current: number
}

export default IMovementSpeed;