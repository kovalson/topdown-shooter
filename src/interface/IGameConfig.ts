interface IGameConfig {
  readonly baseWidth: number
}

export default IGameConfig;