interface IWeaponSprites {
  idle: PIXI.AnimatedSprite,
  run: PIXI.AnimatedSprite,
  attack: PIXI.AnimatedSprite
}

export default IWeaponSprites;