interface IKeysPressed {
  left: boolean,
  up: boolean,
  right: boolean,
  down: boolean,
  shift: boolean,
  leftMouseButton: boolean,
  mouseWheel: number
}

export default IKeysPressed;