interface IGameInfo {
  viewScale: number,
  inverseViewScale: number,
  halfAppWidth: number,
  halfAppHeight: number
}

export default IGameInfo;