import Vector2D from "./Vector";
import IKeysPressed from "../interface/IKeysPressed";
import { timingSafeEqual } from "crypto";

class UserInputManager {
  private _cursor: Vector2D;
  private _keysPressed: IKeysPressed;
  private app: PIXI.Application;
  
  constructor(app: PIXI.Application) {
    this.app = app;
    this._cursor = Vector2D.Zero();
    this._keysPressed = this.initKeysPressed();

    app.view.onmousemove = (event: MouseEvent) => this.mouseMove(event);
    app.view.onmousedown = (event: MouseEvent) => this.mouseDown(event);
    window.onkeydown = (event: KeyboardEvent) => this.keyDown(event);
    window.onkeyup = (event: KeyboardEvent) => this.keyUp(event);
    window.onmouseup = (event: MouseEvent) => this.mouseUp(event);
    window.onmousewheel = (event: Event) => this.mouseWheel(<WheelEvent> event);
  }

  private mouseDown(event: MouseEvent) {
    this._keysPressed.leftMouseButton = true;
  }

  private mouseUp(event: MouseEvent) {
    this._keysPressed.leftMouseButton = false;
  }

  private mouseWheel(event: WheelEvent) {
    this._keysPressed.mouseWheel = Math.sign(event.deltaY);
  }

  private mouseMove(event: MouseEvent) {
    this._cursor = new Vector2D(
      (event.clientX - this.app.view.offsetLeft - this.app.view.clientLeft) / this.app.stage.scale.x,
      (event.clientY - this.app.view.offsetTop - this.app.view.clientTop) / this.app.stage.scale.y
    );
  }

  private keyDown(event: KeyboardEvent) {
		switch (event.keyCode) {
			case 37: case 65: this._keysPressed.left = true; break;
			case 38: case 87: this._keysPressed.up = true; break;
			case 39: case 68: this._keysPressed.right = true; break;
			case 40: case 83: this._keysPressed.down = true; break;
			case 16: this._keysPressed.shift = true; break;
			default: break;
		}
  }

  private keyUp(event: KeyboardEvent) {
		switch (event.keyCode) {
			case 37: case 65: this._keysPressed.left = false; break;
			case 38: case 87: this._keysPressed.up = false; break;
			case 39: case 68: this._keysPressed.right = false; break;
			case 40: case 83: this._keysPressed.down = false; break;
			case 16: this._keysPressed.shift = false; break;
			default: break;
		}
  }

  private initKeysPressed(): IKeysPressed {
    return {
      left: false,
      up: false,
      right: false,
      down: false,
      shift: false,
      leftMouseButton: false,
      mouseWheel: 0
    };
  }

  public get cursor(): Vector2D {
    return this._cursor;
  }

  public set cursor(vector: Vector2D) {
    this._cursor = vector;
  }

  public get keysPressed(): IKeysPressed {
    return this._keysPressed;
  }

  public get movementKeyPressed(): boolean {
    return (
      this._keysPressed.left ||
      this._keysPressed.up ||
      this._keysPressed.right || 
      this._keysPressed.down
    );
  }
}

export default UserInputManager;