class Resources {
  public static textures: PIXI.ITextureDictionary | undefined;
  public static animations: any;

  public static createAnimatedSprite(path: string): PIXI.AnimatedSprite {
    return PIXI.AnimatedSprite.fromFrames(Resources.animations[path]);
  }

  public static load(app: PIXI.Application, callback: Function) {
    app.loader.add("spritesheet", "./spritesheet/spritesheet.json");
    app.loader.load(() => {
      Resources.textures = app.loader.resources["spritesheet"].textures;
      Resources.animations = app.loader.resources["spritesheet"].data.animations

      callback();
    });
  }
}

export default Resources;