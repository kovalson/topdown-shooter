import Tile from "./Tile";

export default {
  Floor: (row: number, column: number, textures: PIXI.ITextureDictionary): Tile => {
    return new Tile(row, column, false, textures["floor1.png"]);
  },
  Wall: (row: number, column: number, textures: PIXI.ITextureDictionary): Tile => {
    return new Tile(row, column, true, textures["wall1.png"]);
  }
};