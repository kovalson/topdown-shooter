import Creature from "./Creature";
import Vector2D from "./Vector";
import PlayerState from "./PlayerState";
import PlayerStates from "./PlayerState/PlayerStates";
import Weapon from "./Weapon";
import Weapons from "./Weapon/Weapons";
import IMovementSpeed from "../interface/IMovementSpeed";
import Size from "./Size";
import Game from "./Game";
import Constants from "./Constants";
import HitBox from "./HitBox";

class Player extends Creature {
  public static readonly INIT_MOVEMENT_SPEED: IMovementSpeed = { min: 0, max: 2, current: 2 };
  public static readonly HIT_BOX_SIZE_FACTOR = 0.5;

  private _feetState: PlayerState;
  private _torsoState: PlayerState;
  
  private _feetSprite: PIXI.AnimatedSprite;
  private _torsoSprite: PIXI.AnimatedSprite;
  
  private _currentWeapon: Weapon;

  constructor(position: Vector2D) {
    super(position);

    this._currentWeapon = Weapons.Knife();

    this._feetState = PlayerStates.FeetIdle(this);
    this._torsoState = PlayerStates.TorsoIdle(this);
    
    this._feetSprite = this._feetState.sprite;
    this._torsoSprite = this._torsoState.sprite;

    this._movementSpeed = Player.INIT_MOVEMENT_SPEED;

    this._moves = false;
    this._attacks = false;

    this._hitbox = new HitBox(Vector2D.Zero(), new Size(30, 30));

    this.updateHitBox();

    this.position = this.position;
    this.angle = this.angle;
  }

  public update(game: Game) {
    this.processUserInput(game);
    this.updatePosition(game.world);
    this.updateHitBox();
    this.updateStates();
  }

  private processUserInput(game: Game) {
    const {cursor, keysPressed, movementKeyPressed} = game.userInput;
    const {left, up, right, down} = keysPressed;
    const {stageOffset} = game;

    this.moves = false;
    this.attacks = false;
    this.velocity = Vector2D.Zero();
    this.angle = this._position.angleWith(cursor.add(stageOffset));

    if (left && !right) this.velocity = new Vector2D(-this._movementSpeed.current, this._velocity.y);
    else if (right && !left) this.velocity = new Vector2D(this._movementSpeed.current, this._velocity.y);

    if (up && !down) this.velocity = new Vector2D(this._velocity.x, -this._movementSpeed.current);
    else if (down && !up) this.velocity = new Vector2D(this._velocity.x, this._movementSpeed.current);

    if (!this.velocity.hasZeroCoordinate) {
      this.velocity = this._velocity.scale(Constants.InverseSQRT_2);
    }

    this.moves = movementKeyPressed;
    this.attacks = keysPressed.leftMouseButton;
  }

  private updateStates() {
    this._feetState.update();
    this._torsoState.update();

    this._feetState = this._feetState.nextState;
    this._torsoState = this._torsoState.nextState;

    this._feetSprite.texture = this._feetState.sprite.texture;
    this._torsoSprite.texture = this._torsoState.sprite.texture;

    this._feetSprite.anchor = this._feetSprite.texture.defaultAnchor;
    this._torsoSprite.anchor = this._torsoSprite.texture.defaultAnchor;
  }

  public get feetSprite(): PIXI.AnimatedSprite {
    return this._feetSprite;
  }

  public get torsoSprite(): PIXI.AnimatedSprite {
    return this._torsoSprite;
  }

  public get currentWeapon(): Weapon {
    return this._currentWeapon;
  }

  public set angle(value: number) {
    this._angle = value;
    this._feetSprite.angle = value;
    this._torsoSprite.angle = value;
  }

  public set position(vector: Vector2D) {
    this._position = vector;
    this._feetSprite.position.set(vector.x, vector.y);
    this._torsoSprite.position.set(vector.x, vector.y);
  }

  public get position(): Vector2D {
    return this._position;
  }
}

export default Player;