import PlayerState from "../PlayerState";
import Player from "../Player";
import Resources from "../Resources";
import PlayerStates from "./PlayerStates";

class FeetRun extends PlayerState {
  constructor(player: Player) {
    super(player, Resources.createAnimatedSprite("feet/run"));
    this.sprite.animationSpeed = 0.7;
  }

  public update() {
    if (!this.player.moves) {
      this.nextState = PlayerStates.FeetIdle(this.player);
    }
  }
}

export default FeetRun;