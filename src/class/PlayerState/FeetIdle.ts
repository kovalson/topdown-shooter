import PlayerState from "../PlayerState";
import Player from "../Player";
import Resources from "../Resources";
import PlayerStates from "./PlayerStates";

class FeetIdle extends PlayerState {
  constructor(player: Player) {
    super(player, Resources.createAnimatedSprite("feet/idle"));
  }

  public update() {
    if (this.player.moves) {
      this.nextState = PlayerStates.FeetRun(this.player);
    }
  }
}

export default FeetIdle;