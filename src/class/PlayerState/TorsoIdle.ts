import PlayerState from "../PlayerState";
import Player from "../Player";
import PlayerStates from "./PlayerStates";

class TorsoIdle extends PlayerState {
  constructor(player: Player) {
    super(player, player.currentWeapon.sprites.idle);
    this.sprite.animationSpeed = 0.5;
  }

  public update() {
    if (this.player.attacks) {
      this.nextState = PlayerStates.TorsoAttack(this.player);
    } else if (this.player.moves) {
      this.nextState = PlayerStates.TorsoRun(this.player);
    }
  }
}

export default TorsoIdle;