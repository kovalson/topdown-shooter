import PlayerState from "../PlayerState";
import Player from "../Player";
import PlayerStates from "./PlayerStates";

class TorsoRun extends PlayerState {
  constructor(player: Player) {
    super(player, player.currentWeapon.sprites.run);
    this.sprite.animationSpeed = 0.5;
  }

  public update() {
    if (this.player.attacks) {
      this.nextState = PlayerStates.TorsoAttack(this.player);
    } else if (!this.player.moves) {
      this.nextState = PlayerStates.TorsoIdle(this.player);
    }
  }
}

export default TorsoRun;