import Player from "../Player";
import FeetIdle from "./FeetIdle";
import FeetRun from "./FeetRun";
import TorsoIdle from "./TorsoIdle";
import TorsoRun from "./TorsoRun";
import TorsoAttack from "./TorsoAttack";

export default {
  FeetIdle: (player: Player) => new FeetIdle(player),
  FeetRun: (player: Player) => new FeetRun(player),

  TorsoIdle: (player: Player) => new TorsoIdle(player),
  TorsoRun: (player: Player) => new TorsoRun(player),
  TorsoAttack: (player: Player) => new TorsoAttack(player)
}