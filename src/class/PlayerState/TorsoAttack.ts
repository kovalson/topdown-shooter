import PlayerState from "../PlayerState";
import Player from "../Player";
import PlayerStates from "./PlayerStates";

class TorsoAttack extends PlayerState {
  private complete: boolean;
  
  constructor(player: Player) {
    super(player, player.currentWeapon.sprites.attack);
    this.complete = false;
    this.sprite.animationSpeed = 0.5;
  }

  public update() {
    this.complete = (this.sprite.currentFrame === 0);

    if (this.complete && !this.player.attacks) {
      if (this.player.moves) {
        this.nextState = PlayerStates.TorsoRun(this.player);
      } else {
        this.nextState = PlayerStates.TorsoIdle(this.player);
      }
    }
  }
}

export default TorsoAttack;