import IWorldConfig from "../interface/IWorldConfig";
import Tile from "./Tile";
import Tiles from "./Tiles";
import Size from "./Size";
import Vector2D from "./Vector";

class World {
  private _array: Array<Array<Tile>> | undefined;
  private _size: Size | undefined;
  private _tileSize: Size | undefined;

  constructor(worldConfig: IWorldConfig) {
    this.generate(worldConfig);
    worldConfig.obstacleBorders && this.generateObstacleBorders(worldConfig.textures);
  }

  private generate(worldConfig: IWorldConfig): void {
    const tiles = [Tiles.Floor, Tiles.Floor, Tiles.Floor, Tiles.Floor, Tiles.Floor, Tiles.Floor, Tiles.Wall];

    this._array = [];
    for (let r = 0; r < worldConfig.rows; r++) {
      this._array[r] = [];
      for (let c = 0; c < worldConfig.columns; c++) {
        this._array[r][c] = tiles[Math.floor(Math.random() * tiles.length)](r, c, worldConfig.textures);
      }
    }

    this._tileSize = new Size(
      this._array[0][0].sprite.texture.width,
      this._array[0][0].sprite.texture.height
    );

    this._size = new Size(
      worldConfig.columns * this._array[0][0].sprite.texture.width,
      worldConfig.rows * this._array[0][0].sprite.texture.height
    );
  }

  private generateObstacleBorders(textures: PIXI.ITextureDictionary): void {
    if (!this._array) return;

		const map = this._array;
		const rows = map.length;
		const cols = map[0].length;

		for (let r = 0; r < rows; r++) {
			this._array[r][0] = Tiles.Wall(r, 0, textures);
			this._array[r][cols - 1] = Tiles.Wall(r, cols - 1, textures);
		}

		for (let c = 0; c < cols; c++) {
			this._array[0][c] = Tiles.Wall(0, c, textures);
			this._array[rows - 1][c] = Tiles.Wall(rows - 1, c, textures);
		}
  }

  public addToStage(stage: PIXI.Container): void {
    const rows = this._array!.length;
    const cols = this._array![0].length;

    for (let r = 0; r < rows; r++) {
      for (let c = 0; c < cols; c++) {
        stage.addChild(this._array![r][c].sprite);
      }
    }
  }

  public getTileByCoordinates(vector: Vector2D): Tile | null {
    const tileSize = this._tileSize!;
    const fx = Math.floor(vector.x / tileSize.width);
    const fy = Math.floor(vector.y / tileSize.height);
    if (fy < 0 || fy >= this.array.length) return null;
    if (fx < 0 || fx >= this.array[fy].length) return null;
    return this.array[fy][fx];
  }

  public get array(): Array<Array<Tile>> {
    return this._array!;
  }

  public get size(): Size {
    return this._size!;
  }

  public get tileSize(): Size {
    return this._tileSize!;
  }
}

export default World;