import Vector2D from "./Vector";
import Size from "./Size";

class HitBox {
  private _position: Vector2D;
  private _size: Size;
  private _halfSize: Size;
  
  private _topLeft: Vector2D;
  private _topRight: Vector2D;
  private _bottomLeft: Vector2D;
  private _bottomRight: Vector2D;
  
  constructor(position: Vector2D, size: Size) {
    this._position = position;
    this._size = size;
    this._halfSize = size.scale(0.5);

    this._topLeft = position;
    this._topRight = position.add(Vector2D.Horizontal(size.width));
    this._bottomLeft = position.add(Vector2D.Vertical(size.height));
    this._bottomRight = position.add(new Vector2D(size.width, size.height));
  }

  public get position(): Vector2D {
    return this._position;
  }

  public set position(vector: Vector2D) {
    this._position = vector;
    this._topLeft = vector;
    this._topRight = vector.add(Vector2D.Horizontal(this.size.width));
    this._bottomLeft = vector.add(Vector2D.Vertical(this.size.height));
    this._bottomRight = vector.add(new Vector2D(this.size.width, this.size.height));
  }

  public get x(): number {
    return this._position.x;
  }

  public get y(): number {
    return this._position.y;
  }

  public get size(): Size {
    return this._size;
  }

  public get width(): number {
    return this._size.width;
  }

  public get height(): number {
    return this._size.height;
  }

  public get halfSize(): Size {
    return this._halfSize;
  }

  public get halfWidth(): number {
    return this._halfSize.width;
  }

  public get halfHeight(): number {
    return this._halfSize.height;
  }

  public get topLeft(): Vector2D {
    return this._topLeft;
  }

  public get topRight(): Vector2D {
    return this._topRight;
  }

  public get bottomLeft(): Vector2D {
    return this._bottomLeft;
  }

  public get bottomRight(): Vector2D {
    return this._bottomRight;
  }
}

export default HitBox;