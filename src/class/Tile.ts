import Vector2D from "./Vector";

type Textures = PIXI.Texture | Array<PIXI.Texture>;

class Tile {
  public readonly position: Vector2D;
  public readonly row: number;
  public readonly column: number;
  public readonly obstacle: boolean;
  public readonly sprite: PIXI.Sprite;

  constructor(row: number, column: number, obstacle: boolean, textures: Textures) {
    this.row = row;
    this.column = column;
    this.obstacle = obstacle;
    
    const texture = (Array.isArray(textures)) ? textures[Math.random() * textures.length | 0] : textures;
    
    this.sprite = new PIXI.Sprite(texture);
    this.sprite.position.set(column * texture.width, row * texture.height);

    this.position = new Vector2D(column * texture.width, row * texture.height);
  }
}

export default Tile;