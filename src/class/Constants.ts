class Constants {
  
  // Math.PI / 180
  public static readonly PIDiv180 = 0.017453292519943295;

  // 180 / Math.PI
  public static readonly InversePIDiv180 = 57.29577951308232;

  // sqrt(2)
  public static readonly SQRT_2 = 1.4142135623730951;

  // 1 / sqrt(2)
  public static readonly InverseSQRT_2 = 0.7071067811865475;

  // camera smoothness factor while centering on target
  public static readonly CameraSmoothnessFactor = 0.047619047619047616;

  // camera smoothness factor while moving mouse over the canvas
  public static readonly CameraMouseMovementFactor = 0.2857142857142857;
  
}

export default Constants;