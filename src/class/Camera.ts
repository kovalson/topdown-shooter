import Constants from "./Constants";
import Vector2D from "./Vector";
import Game from "./Game";
import GameObject from "./GameObject";

class Camera {
  private app: PIXI.Application;
  private _offsetMax: Vector2D;
  private _target: GameObject | undefined;
  
  constructor(app: PIXI.Application) {
    this.app = app;
    this._offsetMax = Vector2D.Zero();
  }

  public update(game: Game) {
    const {CameraSmoothnessFactor, CameraMouseMovementFactor} = Constants;
    const {halfAppWidth, halfAppHeight, inverseViewScale} = game.info;
    const {stage} = this.app;
    const {cursor} = game.userInput;
    const player = game.player;

    const center = new Vector2D(
      halfAppWidth * inverseViewScale + this.app.stage.pivot.x,
      halfAppHeight * inverseViewScale + this.app.stage.pivot.y
    );

    const diff = new Vector2D(
      center.x - player.position.x + ((halfAppWidth - cursor.x) * CameraMouseMovementFactor),
      center.y - player.position.y + ((halfAppHeight - cursor.y) * CameraMouseMovementFactor)
    );

    stage.pivot.y -= Math.round(diff.y * CameraSmoothnessFactor);
    stage.pivot.x -= Math.round(diff.x * CameraSmoothnessFactor);

    stage.pivot.x = Math.max(stage.pivot.x, 0);
    stage.pivot.y = Math.max(stage.pivot.y, 0);
    stage.pivot.x = Math.min(stage.pivot.x, this._offsetMax.x);
    stage.pivot.y = Math.min(stage.pivot.y, this._offsetMax.y);
  }

  public get target(): GameObject {
    return this._target!;
  }

  public set target(gameObject: GameObject) {
    this._target = gameObject;
  }

  public get offsetMax(): Vector2D {
    return this._offsetMax;
  }

  public set offsetMax(vector: Vector2D) {
    this._offsetMax = vector;
  }
}

export default Camera;