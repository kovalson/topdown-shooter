import Player from "./Player";

abstract class PlayerState {
  private _player: Player;
  private _sprite: PIXI.AnimatedSprite;
  private _nextState: PlayerState;
  
  constructor(player: Player, sprite: PIXI.AnimatedSprite) {
    this._player = player;
    this._sprite = sprite;
    this._nextState = this;

    this.initSprite();
  }

  abstract update(): void;

  private initSprite(): void {
    this._sprite.gotoAndPlay(0);
  }

  public get sprite(): PIXI.AnimatedSprite {
    return this._sprite;
  }

  public get player(): Player {
    return this._player;
  }

  public get nextState(): PlayerState {
    return this._nextState;
  }

  public set nextState(state: PlayerState) {
    this._nextState = state;
  }
}

export default PlayerState;