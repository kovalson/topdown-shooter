import IGameConfig from "../interface/IGameConfig";
import * as PIXI from "pixi.js";
import World from "./World";
import Player from "./Player";
import Vector2D from "./Vector";
import UserInputManager from "./UserInputManager";
import Resources from "./Resources";
import Camera from "./Camera";
import IGameInfo from "../interface/IGameInfo";
import DebugManager from "./DebugManager";
import TimeCounter from "./debug/TimeCounter";

class Game {
  private readonly ASPECT_RATIO: number = (4 / 3);
  private readonly baseWidth: number;

  private app: PIXI.Application;
  private _info: IGameInfo;
  private camera: Camera;
  
  private _world: World | undefined;
  private _player: Player | undefined;
  private _userInput: UserInputManager;

  constructor(config: IGameConfig) {
    this.baseWidth = config.baseWidth;
    this.app = this.initPixiApplication();
    this.camera = new Camera(this.app);
    this._info = {
      viewScale: 0,
      inverseViewScale: 0,
      halfAppWidth: this.app.view.width / 2,
      halfAppHeight: this.app.view.height / 2
    };
    this._userInput = new UserInputManager(this.app);
    
    Resources.load(this.app, () => {
      this.init();
    });

    DebugManager.Init();
  }

  private update(delta: number) {
    DebugManager.Get("FPS Counter").start();
    this.player!.update(this);
    this.camera.update(this);
    DebugManager.Get("FPS Counter").end();
    DebugManager.Show("FPS Counter");
  }

  private init(): void {
    this._world = new World({
      rows: 64,
      columns: 64,
      textures: Resources.textures!,
      obstacleBorders: true
    });

    this._player = new Player(new Vector2D(600, 600));
    this._userInput.cursor = this._player.position;

    this.camera.target = this._player;

    this.world.addToStage(this.app.stage);
    this.app.stage.addChild(this._player.feetSprite);
    this.app.stage.addChild(this._player.torsoSprite);

    this.app.ticker.add((delta: number) => this.update(delta));

    this.preserveAspectRatio();
    DebugManager.AddDebugInfo("FPS Counter", new TimeCounter(60));
  }

  private initPixiApplication(): PIXI.Application {
    const app = new PIXI.Application();
    app.view.id = "canvas";
    document.body.appendChild(app.view);
    window.addEventListener("resize", () => this.preserveAspectRatio());
    return app;
  }

  private preserveAspectRatio(): void {
    const browserHeight = Math.max(document.documentElement.clientHeight, document.body.clientHeight);
    const width = this.ASPECT_RATIO * browserHeight;
    const height = browserHeight;

    this.app.renderer.resize(width, height);
    this._info.viewScale = (width / this.baseWidth);
    this._info.inverseViewScale = (1 / this._info.viewScale);
    this.app.stage.scale.x = this.app.stage.scale.y = this._info.viewScale;

    this._info.halfAppWidth = (this.app.view.width / 2);
    this._info.halfAppHeight = (this.app.view.height / 2);

    this.camera.offsetMax = new Vector2D(
      this.world!.size.width - this.app.view.width * this._info.inverseViewScale,
      this.world!.size.height - this.app.view.height * this._info.inverseViewScale
    );

    this.app.stage.position.x = Math.floor(this.app.stage.position.x);
    this.app.stage.position.y = Math.floor(this.app.stage.position.y);
  }

  public get world(): World {
    return this._world!;
  }

  public get player(): Player {
    return this._player!;
  }

  public get info(): IGameInfo {
    return this._info;
  }
  
  public get userInput(): UserInputManager {
    return this._userInput;
  }

  public get stageOffset(): Vector2D {
    return new Vector2D(
      this.app.stage.pivot.x,
      this.app.stage.pivot.y
    );
  }
}

export default Game;