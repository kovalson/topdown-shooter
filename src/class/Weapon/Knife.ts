import Weapon from "../Weapon";
import Resources from "../Resources";

class Knife extends Weapon {
  constructor() {
    super({
      idle: Resources.createAnimatedSprite("knife/idle"),
      run: Resources.createAnimatedSprite("knife/run"),
      attack: Resources.createAnimatedSprite("knife/attack")
    });
  }
}

export default Knife;