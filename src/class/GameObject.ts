import Vector2D from "./Vector";
import Size from "./Size";
import HitBox from "./HitBox";

abstract class GameObject {
  protected _position: Vector2D;
  protected _hitbox: HitBox;
  protected _collidable: boolean;
  protected _collides: boolean;

  constructor(position: Vector2D) {
    this._position = position;
    this._hitbox = new HitBox(Vector2D.Zero(), Size.Zero());
    this._collidable = false;
    this._collides = false;
  }

  public updateHitBox(): void {
    this._hitbox.position = new Vector2D(
      this._position.x - this._hitbox.halfSize!.width,
      this._position.y - this._hitbox.halfSize!.height
    );
  }

  public get position(): Vector2D {
    return this._position;
  }

  public set position(vector: Vector2D) {
    this._position = vector;
  }
  
  public get hitbox(): HitBox {
    return this._hitbox;
  }

  public get collidable(): boolean {
    return this._collidable;
  }

  public set collidable(bool: boolean) {
    this._collidable = bool;
  }

  public get collides(): boolean {
    return this._collides;
  }

  public set collides(bool: boolean) {
    this._collides = bool;
  }
}

export default GameObject;