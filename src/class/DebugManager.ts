class DebugManager {
  private static debugElement: HTMLDivElement;
  private static objects: Map<string, any> = new Map();
  private static elements: Map<string, HTMLDivElement> = new Map();

  public static Init(debugElementId: string = "debug"): void {
    const debugElement = document.createElement("div");
    
    debugElement.id = debugElementId;
    debugElement.style.position = "absolute";
    debugElement.style.left = "15px";
    debugElement.style.top = "15px";
    debugElement.style.backgroundColor = "#000";
    debugElement.style.color = "#fff";
    debugElement.style.fontFamily = "Consolas";

    document.body.appendChild(debugElement);
    this.debugElement = debugElement;
  }

  public static AddDebugInfo(name: string, object: any): void {
    const infoElement = document.createElement("div");

    infoElement.id = name.replace(" ", "");
    
    this.debugElement.appendChild(infoElement);
    this.objects.set(name.replace(" ", ""), object);
    this.elements.set(name.replace(" ", ""), infoElement);
  }

  public static Get(name: string): any {
    return this.objects.get(name.replace(" ", ""));
  }

  public static Show(name: string): void {
    const contents = this.objects.get(name.replace(" ", "")).toString();
    this.elements.get(name.replace(" ", ""))!.innerHTML = name + ": "+ contents;
  }

  public static Print(name: string, data: string): void {
    const filtered = name.replace(" ", "");
    
    if (!this.elements.get(filtered)) {
      const infoElement = document.createElement("div");

      infoElement.id = filtered;
      
      this.debugElement.appendChild(infoElement);
      this.elements.set(filtered, infoElement);
    }

    this.elements.get(filtered)!.innerHTML = name + ": " + data;
  }
}

export default DebugManager;