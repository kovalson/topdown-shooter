import Constants from "./Constants";

class Vector2D {
  public static Zero(): Vector2D {
    return new Vector2D(0, 0);
  }

  public static Horizontal(x: number): Vector2D {
    return new Vector2D(x, 0);
  }

  public static Vertical(y: number): Vector2D {
    return new Vector2D(0, y);
  }

  constructor(
    public readonly x: number,
    public readonly y: number
  ) {}

  public angleWith(vector: Vector2D): number {
    return Math.atan2(vector.y - this.y, vector.x - this.x) * Constants.InversePIDiv180;
  }

  public add(vector: Vector2D): Vector2D {
    return new Vector2D(
      this.x + vector.x,
      this.y + vector.y
    );
  }

  public scale(scalar: number): Vector2D {
    return new Vector2D(
      this.x * scalar,
      this.y * scalar
    );
  }

  public get isZero(): boolean {
    return (this.x === 0 && this.y === 0);
  }

  public get hasZeroCoordinate(): boolean {
    return (this.x === 0 || this.y === 0);
  }

  public get pointsLeft(): boolean {
    return (this.x < 0);
  }

  public get pointsUp(): boolean {
    return (this.y < 0);
  }

  public get pointsRight(): boolean {
    return (this.x > 0);
  }

  public get pointsDown(): boolean {
    return (this.y > 0);
  }
}

export default Vector2D;