import GameObject from "./GameObject";
import Vector2D from "./Vector";
import IMovementSpeed from "../interface/IMovementSpeed";
import Game from "./Game";
import Tile from "./Tile";
import World from "./World";

abstract class Creature extends GameObject {
  protected _angle: number;
  protected _velocity: Vector2D;
  protected _movementSpeed: IMovementSpeed;
  protected _moves: boolean;
  protected _attacks: boolean;
  
  constructor(position: Vector2D) {
    super(position);
    this._angle = 0;
    this._velocity = Vector2D.Zero();
    this._movementSpeed = {
      min: 0,
      max: 0,
      current: 0
    };
    this._moves = false;
    this._attacks = false;
  }

  abstract update(game: Game): void;

  protected updatePosition(world: World): void {
    if (this.velocity.isZero) {
      return;
    }

    const hitbox = this._hitbox;
    let nx = this._position.x;
    let ny = this._position.y;
    let tile1: Tile | null;
    let tile2: Tile | null;
    let collides = false;
    this._collides = false;

    // test horizontally
    // if creature is moving left
    if (this.velocity.pointsLeft) {
      tile1 = world.getTileByCoordinates(hitbox.topLeft.add(Vector2D.Horizontal(this.velocity.x)))!;
      tile2 = world.getTileByCoordinates(hitbox.bottomLeft.add(Vector2D.Horizontal(this.velocity.x)))!;
      collides = (tile1.obstacle || tile2.obstacle);
      this.collides = (collides || this._collides);

      if (collides) nx = (tile1.column + 1) * world.tileSize.width + hitbox.halfWidth;
      else nx += this.velocity.x;
    }

    // else if creature is moving right
    if (this.velocity.pointsRight) {
      tile1 = world.getTileByCoordinates(hitbox.topRight.add(Vector2D.Horizontal(this.velocity.x)))!;
      tile2 = world.getTileByCoordinates(hitbox.bottomRight.add(Vector2D.Horizontal(this.velocity.x)))!;
      collides = (tile1.obstacle || tile2.obstacle);
      this.collides = (collides || this._collides);

      if (collides) nx = (tile1.position.x - hitbox.halfWidth - 1);
      else nx += this.velocity.x;
    }

    // text vertically
    // if creature is moving up
    if (this.velocity.pointsUp) {
      tile1 = world.getTileByCoordinates(hitbox.topLeft.add(Vector2D.Vertical(this.velocity.y)))!;
      tile2 = world.getTileByCoordinates(hitbox.topRight.add(Vector2D.Vertical(this.velocity.y)))!;
      collides = (tile1.obstacle || tile2.obstacle);
      this.collides = (collides || this._collides);

      if (collides) ny = (tile1.row + 1) * world.tileSize.height + hitbox.halfHeight;
			else ny += this.velocity.y;
    }

    // else if creature is moving down
    else if (this.velocity.pointsDown) {
      tile1 = world.getTileByCoordinates(hitbox.bottomLeft.add(Vector2D.Vertical(this.velocity.y)))!;
      tile2 = world.getTileByCoordinates(hitbox.bottomRight.add(Vector2D.Vertical(this.velocity.y)))!;
      collides = (tile1.obstacle || tile2.obstacle);
      this.collides = (collides || this._collides);

			if (collides) ny = tile1.position.y - hitbox.halfHeight - 1;
			else ny += this.velocity.y;
    }

    this.position = new Vector2D(nx, ny);
  }

  public get angle(): number {
    return this._angle;
  }

  public set angle(value: number) {
    this._angle = value;
  }

  public get velocity(): Vector2D {
    return this._velocity;
  }

  public set velocity(vector: Vector2D) {
    this._velocity = vector;
  }

  public get movementSpeed(): IMovementSpeed {
    return this._movementSpeed;
  }

  public get moves(): boolean {
    return this._moves;
  }

  public set moves(bool: boolean) {
    this._moves = bool;
  }

  public get attacks(): boolean {
    return this._attacks;
  }

  public set attacks(bool: boolean) {
    this._attacks = bool;
  }
}

export default Creature;