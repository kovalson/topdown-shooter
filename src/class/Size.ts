class Size {
  public static Zero(): Size {
    return new Size(0, 0);
  }

  constructor(
    public readonly width: number,
    public readonly height: number
  ) {}

  public scale(scalar: number): Size {
    return new Size(
      this.width * scalar,
      this.height * scalar
    );
  }
}

export default Size;