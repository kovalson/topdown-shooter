class TimeCounter {
  private _startTime: number;
  private _endTime: number;
  private _executionTime: number;

  private _averageExecutionTime: number;
  private times: number[];
  private timesNumber: number;
  private currentTimeNumber: number;
  private _min: number;
  private _max: number;

  constructor(timesNumber: number = 0) {
    this._startTime = 0;
    this._endTime = 0;
    this._executionTime = 0;

    this._averageExecutionTime = 0;
    this.times = [];
    this.timesNumber = timesNumber;
    this.currentTimeNumber = 0;

    this._min = Infinity;
    this._max = -Infinity;
  }

  public start(): void {
    this._startTime = performance.now();
  }

  public end(): void {
    this._endTime = performance.now();
		this._executionTime = (this._endTime - this._startTime);

		if (this.timesNumber > 0) {
      this.times[this.currentTimeNumber] = this._executionTime;

      this._min = Math.min(this._min, this._executionTime);
      this._max = Math.max(this._max, this._executionTime);
  
			this.currentTimeNumber++;
			if (this.currentTimeNumber >= this.timesNumber) {
				this.currentTimeNumber = 0;
				this._averageExecutionTime = (this.times.reduce((a, b) => a + b, 0) / this.timesNumber);
			}
		}
  }

  public get time(): number {
    return this._executionTime;
  }

  public get averageTime(): string {
    return this._averageExecutionTime.toFixed(2);
  }

  public get min(): number {
    return this._min;
  }

  public get max(): number {
    return this._max;
  }

  public toString(): string {
    const avg = parseFloat(this.averageTime);
    return (
      "<br>" + 
      "- " + Math.round(1000 / avg) + " FPS<br>" +
      "- " + avg + " ms / frame<br>" + 
      "- " + "min: " + this.min + "<br>" +
      "- " + "max: " + this.max
    );
  }
}

export default TimeCounter;