import IWeaponSprites from "../interface/IWeaponSprites";

abstract class Weapon {
  private _sprites: IWeaponSprites;
  
  constructor(sprites: IWeaponSprites) {
    this._sprites = sprites;
  }

  public get sprites(): IWeaponSprites {
    return this._sprites;
  }
}

export default Weapon;